package ru.t1.dkandakov.tm.api;

import ru.t1.dkandakov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}