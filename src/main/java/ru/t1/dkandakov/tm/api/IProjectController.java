package ru.t1.dkandakov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}