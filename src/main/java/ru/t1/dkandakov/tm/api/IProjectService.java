package ru.t1.dkandakov.tm.api;

import ru.t1.dkandakov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}