package ru.t1.dkandakov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}